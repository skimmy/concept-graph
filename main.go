package main

import (
	"fmt"
)

var InitialConceptCapacity = 128

func main() {
	cg := ConceptGraph{
		concepts: make(map[string]*GraphNode, InitialConceptCapacity),
	}
	cg.NewConcept("Blue", []string{})
	cg.NewConcept("Sky", []string{"Blue"})
	cg.NewConcept("Ocean", []string{"Blue", "Sky"})
	cg.AddNeighborsTo(cg.GetNode("Blue"), []string{"Sky", "Ocean"})
	cg.AddNeighborsTo(cg.GetNode("Sky"), []string{"Ocean"})
	for _,v := range cg.concepts {
		fmt.Printf("%s\n  -> ", v.concept)
		for i := range v.neighbors {
			fmt.Printf("  %s", v.neighbors[i].concept)
		}
		fmt.Println()
	}
	fmt.Printf("Graph:     %v\n", cg)
}
