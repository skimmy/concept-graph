package main


type GraphNode struct {
	neighbors []*GraphNode
	concept	  string
}

type ConceptGraph struct {
	concepts map[string]*GraphNode
}

func (g ConceptGraph) NewConcept(concept string, relations []string) (*GraphNode, error) {
	newNode := GraphNode{
		neighbors: make([]*GraphNode, len(relations)),
		concept: concept,
	}
	for i := range relations {
		newNode.neighbors[i] = g.concepts[relations[i]]
	}
	g.concepts[concept] = &newNode
	return &newNode, nil
}

func (g ConceptGraph) GetNode(concept string) *GraphNode {
	return g.concepts[concept]
}

func (g ConceptGraph) AddNeighborsTo(node *GraphNode, concepts []string) bool {
	for i := range concepts {
		node.neighbors = append(node.neighbors, g.concepts[concepts[i]])
	}
	return true
}

func (n GraphNode) AddNeighbor(neighbor *GraphNode) bool {
	n.neighbors = append(n.neighbors, neighbor)
	return true
}
